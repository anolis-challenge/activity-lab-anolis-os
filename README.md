## 介绍

龙蜥一刻 - 体验Anolis OS任务的PR提交仓库


## 活动的操作步骤

详见Anolis OS体验任务的[帮助文档](https://gitee.com/anolis-challenge/summer2022/blob/master/%E9%BE%99%E8%9C%A5%E4%B8%80%E5%88%BB/AnolisOS%E4%BD%93%E9%AA%8C%E4%BB%BB%E5%8A%A1/Anolis%20OS%20%E4%BD%93%E9%AA%8C%E4%BB%BB%E5%8A%A1%E5%8F%8A%E7%BB%93%E6%9E%9C%E6%8F%90%E4%BA%A4%E6%B5%81%E7%A8%8B.md)。

注意：完成体验后，要在本仓库中提交PR。否则，无法获得贡献值。